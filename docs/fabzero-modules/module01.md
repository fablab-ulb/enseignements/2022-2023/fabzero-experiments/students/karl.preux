# 1. Gestion de projet et documentation

Durant la première semaine, l'installation des différents élements afin de creer notre blog, documenter les différentes recherches et travaux et pouvoir en profiter sur notre propre ordinateur et l'uploader sur directement sur le site. 

## 1.2 La documentation

L'objectif de documenter n'importe quel travail ou action est de pouvoir garder une trace de l'apprentissage, des erreurs et des solutions qui ont pu apparaitre durant notre avancée jusqu'au point final du projet. Celle-ci permet également au futur étudiant mais aussi au collègue de pouvoir s'inspirer ou s'aider lorsqu'ils rencontrent les mêmes problèmes.
C'est également une page qui nous permet de nous souvenir des différents procédées utilisées. 

### 1.2.1 Les élements utiles à la documentation

Premièrement, un éditeur de texte est des élements principaux au traitement de textes. Dans mon cas, Notepad++ était déjà installer sur mon ordinateur. Maintenat un nouveau type de language nommé **Markdown** sera utilisé. Un language assez simple avec quelque code de ponctuation à connaitre. 
par exemple ici nous avons quelques ligne de code repris du tuto :
![](../images/code_exemple.jpg)

- [Markdown](https://en.wikipedia.org/wiki/Markdown)

### 1.2.2 Deployement automatique et version control

#### 1.2.2.1 GITLAB
L'utilisation de **GITLAB** est important car il permet: 

1. un dépot d'archives
2. controle de version **(GIT)**
3. régler des problème 
4. CI/CD

Mais également de updater nos différent document et page web.  Pour cela, il faut 

 1. se connecter à ***GITLAB***
 2. se mettre sur son propre profil 
 3. modifier les fichiers markdown (.md)

Afin de travailler sur mon ordinateur et donc éviter de rester connecter sur le serveur **GITLAB** , on réalisera une copie du projet sur notre ordinateur personel pour cela  doit installer **GI BASH** sur notre ordinateur. Après cela quelque commande sont nécessaire à fin de se connecter. 
Le tutoriel est vissible sur 
[Connection](https://docs.github.com/en/get-started/getting-started-with-git/setting-your-username-in-git).

#### 1.2.2.2 GIT BASH

1. Ouvrir **GIT BASH** 
2. Entrer son username : 

```
$ git config --global user.name "Mona Lisa"
```

3. Confirmer et vérifier que le changement est correcte: 
```
$ git config --global user.name
```
la même chose peut être fait pour un single repository donc en local on peut choisir notre chemin grâce à cd et ensuite rentrer les meme ligne de commande en éliminant le *global*.

### 1.2.3 SSH Key

Maintenant, on peut créer une connection sécurisé entre notre ordinateur et le serveur **GITLAB**." GitLab utilise le protocole SSH pour communiquer de manière sécurisée avec Git. Lorsque vous utilisez des clés SSH pour vous authentifier auprès du serveur distant GitLab, vous n'avez pas besoin de fournir votre nom d'utilisateur et votre mot de passe à chaque fois."

Nous utiliserons une **clé ED25519** SSH keys car elle décrite comme la plus sécure et performante. 
Pour générer une SSH key on peut suivre le lien suivant :
- [SSH](https://docs.gitlab.com/ee/user/ssh.html)

Où tout les étapes sont bien décrite:

1. utilisé la commande :
    ```
     ssh-keygen -t ed25519 -C "<comment>"
     ```

    en remplacent "comment" par notre adresse email. 

2. une reponse similaire à celle-ci est générer :
    ```
     Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/user/.ssh/id_ed25519):
    ```
3. Presser sur Enter et un dossier sera créer.

4. Specifier un mot de passe ( celui-ci est également invisible pour vous):
    ```
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    ```

5. une public et private key sont générer. il faut ajouter la clé publique  son **GITLAB** accountout le cheminement est décrit ici:
    - [public key ](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)

La dernière étape est de vérifier la connection qui est également décrit à la suite des étape précédentes sur le site web fournie. 

### 1.2.4  Enregistrer nos modifications 

1. Rentrer dans notre dossier grâce à la commande : *cd* suivi d'un copier coller de l'addresse de notre dossier 

| Commande |  Description    |  
|-----|-----------------|
| git pull   | Télécharger la dernière version du projet    |  
| git add -A  | Ajouter tous les changements que nous avons réalisés    |  
| git commit -m "Message"  | Parvenir un message d'explication àfin de décrire nos modification  |  
| git push   | Envoyer nos changement sur le serveur   |  
| git status   | Evaluer le status   | 

### 1.2.5 Mkdocs

Dans un file nommer mKdocs plusieurs étapes sont à modifier telque rentrer les liens de notre page fablab sur gitlab et sur fabzeros.

Il est également possible de placer un thème avant de modifier notre page web. 

### 1.3 Photo, videos

Pour ne pas prendre trop de placer sur l'ordinateur ou sur la page web les photos sont retouchées et zipper, compresser à fin de réduire la taille de stockage. On peut utiliser un outil comme [gimp](https://www.gimp.org/downloads/) et on y trouve un [tutoriel](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-dimensions-of-an-image-scale).

### 1.4 Gestion de projet

Pour garder le rythme et avancer en continue dans mon travail, j'essaye de mettre mon profil à jour régulièrement le jour après le cours àfin de garder les idées au frais. Durant les cours les idées les plus importantes sont mises directement sur un fichier texte. 

