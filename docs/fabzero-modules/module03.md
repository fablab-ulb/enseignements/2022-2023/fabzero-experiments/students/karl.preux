# 3. Impression 3D


## L'objectif

le but de cette semaine était de pouvoir identifier les avantages et limitations de l'imprimante 3D. Ensuite, d'appliquée les méthodes de la séance précédentes à un processus d'impression.  
Ainsi de pouvoir fournir un skills et nous ouvrir l'accès libre au imprimante qui est justifier par cette formation.

### Avant la formation


1. Installer [Slicing software](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) à partir de [PrusaSlicer](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.237927488.1554251034.1678271016-1872888702.1678271016) ce programme permet la conversion d'objet numérique 3D en instruction spécifique pour l'imprimante 3D. Donc on transforme un fichier sous format [STL](https://universitelibrebruxelles-my.sharepoint.com/:u:/g/personal/karl_preux_ulb_be/EeHtTCXHwSFDqIv9UciVqQsBOiLhJKnnTsbP_QqnL4Fy2w?e=LDYfgw) en un format g-code
qui est lisisble sous une carte SD par l'imprimante qui transforme en opération mécanique. 

2. Quelque règles de facilité sont donnée à travers ce [3D Tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md), comme 
mettre en mode Avancé ou expert àfin de bénéficier de tout les avantages du logiciel comme les réglages.
Ensuite des règles de préparation du G-code sont données tels que l'importation du fichier ou l'orientation du modèle, selection de l'imprimante, selection du filament et hauteur de couches, etc,...

3. On peut avoir plusieur problèmes qui se révèlent assez courant tels que: mauvaise adhérence de l'impression au plateau qui peut amener à un échec de l'impression du au décollement de la pièce ou encore pire à mise en hors service de celle-ci. Les différentes précaution à prendre sont :
    - ajouter une bordure dans le slicer ayant un faible surface de base, 
    - vérifier s'il n'y apas de résidus de plastique sous le plateau, 
    - nettoyer le plateau avant tout opération, 
    - à chaque changement de filament, calibrer la [hauteur de 1ère couche](https://help.prusa3d.com/fr/article/calibration-de-la-premiere-couche-i3_112364). les problèmes suivants concernes les noeuds dans la bobine de filament ou encore un version obselète du logiciel slicer/firmware. 

#### Useful links

- [Design d'une pièce en vue de l'imprimer en 3D](https://help.prusa3d.com/fr/article/modelisation-en-pensant-a-limpression-3d_164135)
- [Matériaux de filaments d'impression 3D](https://help.prusa3d.com/fr/category/guide-des-materiaux_220)
- [Torture test](https://www.thingiverse.com/thing:2806295)
- [CNC Kitchen Youtube channel](https://www.youtube.com/@CNCKitchen)

## My work

### Choix du matériaux 
Au vus de mon objet designé dans le module 2, le choix parfait serait ABS car cet un matériaux qui convient à l'impréssion de pièce sollicitées mécaniquement, donc qui ont une haute ténacité et resistant à la température mais le polycarbonate est aussi un élément qui convient à ce type d'objet.
Mais le PETG ou le PLA sont des matériaux facile à imprimer et à utilisés et ils sont également peu couteux et adaptés à des simples impressions, ils bénéficient d'une tenacité et PLET d'une résistance à des températures élevées mais pas le PLA qui lui se féforme à environ 60° mais on commencera par une impression avec ce matériau pour réalisé cette première impression.

En suivant les instruction les parmètres suivant ont été modifiés ou sont déjà données par défault:
Pour ici, la haueur de couches on peut laissé les hauteur par défault

![](../fabzero-modules/images/para_13D.jpg)

Par exemple ici, nous avons le remplissage, nous pouvons prendre entre 25% et 30% pour une structure plus solide. 

![](../fabzero-modules/images/parametre_3D.jpg)

Nous avons également choisis un motif en nid d'abeille pour avoir une structure solide. 
On peu utiliser aussi des supports et des bordures pour minimiser la quantité de matérieux utilisés mais cela ne sera pas fait ici.
On peut définire également notre mattériaux donc PLA pour nous. 
La définition de l'échelle de notre structure est réalisé ensuite pour limiter notre temps d'impression.
Grâce au hoverhang test on a pu savoir à partir de quel angle la structure devient imparfaite. Cet angle est de 60°. 

![](../fabzero-modules/images/module_3_pic_1.jpg)

Le temps d'impression peut être estimer ainsi que la découpe couche par couche peut être visualisé. 

Ensuite, il reste à exporter le fichier sous forme de g-code et mettre sur une carte SD du fablab et puis lancé l'impression.

![](../fabzero-modules/images/IMG_4352.jpg)
![](../fabzero-modules/images/IMG_4351.jpg)

 