# 2. Conception Assistée par Ordinateur (CAO)

Le but de cette semaine était de connaitre rencontrer les différents type de programmes àfin de réalisé une conception assisté par ordinateur. Qui serviront ensuite à modélier un kit choisi àfin d'être imprimer. 
## 2.1 Les différents mécanismes

### 2.1.1 Compliant mechanism
Avec la définition suivante : "A mechanism is a mechanical device used to transfer or transform motion, force, or energy. Traditional rigid-body mechanisms consist of rigid links connected at movable joints. Two examples are shown below."

Ce mecanisme permet une certaines déformation sur sa structures grace à des degrés de libertés laissés sans contraintes. On peut retouver une translation ou une rotation ou plus d'une dans la direction désirée. 
Tout les différents avantages de ce mécanisme peuvent être retrouvé sur le lien suivant - [Compliant mechanism](https://www.compliantmechanisms.byu.edu/about-compliant-mechanisms). 
### 2.1.2 Bistable mechanism
Bistable vient du fait que le system a deux equilibre stable donc par une certaine modification il peut passer d'un certain état stable à un autre état stabe. 
Il est possible de creer un tel mechanism à l'aide d'imprimante 3D et cela grace au différent type de logiciel de construction numérique. 

## 2.2 Logiciel numérique 
On retroue énormement de logiciel 3D sur le net, tout d'abord, on retrouve les logiciel dit matricielles qui traitent les images par pixels où toutes images est un ensembles infini de point de couleurs. 
et les logiciel vectoriels ou l'images est plus unies et les limites sont possés par la géométrie vectoriel. 

## 2.2.1 Logiciel matriciel 

1. Paint
2. GIMP
3. Blender

Ici, les pixels peuvent être modifié ainsi que le contraste, l'ajout de filtre peut etre réaliser également. 

## 2.2.2 Logiciel vectoriel

Comme stipulé plus haut, les images vectoriels sont basées sur de la géométrie vectoriel dont des droites et des formes diverses tel que des polugones et des arc de cercles et autres geométries. 
Ces images sont plus légères que les images matriciels et les modifications sont plus faciles et rapides. 
On retrouve plusieurs programmes comme: 

1. Inskape
2. Openscad
3. Fusion360
4. Solidworks 
5. FreeCAD
6. et encore plein d'autres ...

Parmis les logiciels décrit certains nécessites un accès payant ou gratuit via une license universitaire d'autre sont considérés comme opensources donc ouvert à tous. 
L'utilités d'utilisé un des programmes précédent est de pouvoir transmettre un chemin droit et suivis à l'imprimantes 3D lorsque la pièces sera réalisée ce qui n'est pas vraiment possible avec un logiciel matriciel. 

Ayant travaillé sur Openscad, une description rapide sera faite :

### 2.2.2.1 OpenSCAD

Ici, un petit temps d'adaptation à un style de C++ est nécessaire mais facile et rapide car ce programe fonctionne grâce à du code donc on lui décrit directement ce que l'on souhaite faire sous forme de code. 
Toute les fonction utile à se programme se retrouve sur le - [Cheatsheet](https://openscad.org/cheatsheet/).  

#### function of OpenSCAD:
 1. different forme comme cube, sphere,...
 
##### Operateurs:
 1. union() permet d'unir deux forme. 
 2. difference() permet de faire une difference entre les deux formes (une soustraction, donc extraire une partie).
 3. hull() permet de lier les deux forme d'une certaine manière sans voir les différence entre les formes.
 4. Minkowski() permet de creer une forme qui respecte certaines lois. 
 
##### Transformation : 
 1. rotate() qui permet une rotation en donnant les différent argument en x,y,z
 2. translate() qui permet la translation celle-ci peut également se faire selon les trois axes de liberté
 la combinaisson des deux est également possible. 
 
##### Animation: 
 1. $t introduit dans la fonction de rotation va permettre une rotation continue selo un axe. Ensuite cliquer sur vue et ensuite animation, puis définir le nombre d'image par seconde et le nombre d'étape. 
 
 Un exemple de chacune de ces fonctions est donner sur la page web ici - [Exemple OpenSCAD](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/OpenSCAD.md).
 
 Beaucoup d'informations peuvent être retrouvé dans - [Cheatsheet](https://openscad.org/cheatsheet/). 
 
##### Formes géométriques 

| 2D |  3D   | arguemnts  |  
|-----|-----------------|---------|
| circle()  | sphere()    |  radius ou d= diameter| 
| square()  | cube()    |  size or (x,y,z) distance et center | 
| square()  | cube()  |  [width, (depth),height], center|
| polygon()  |    |  [points] or [points],[paths]| 
|    | cylinder  |  h=height,r=radius, center| 

** Important 
Il est important de bien paramétrisé ses fonction àfin de gagner du temps sur une retouche ou une modification àfin de correspondre à une autre forme. 


 
## 2.3 Licences

On rerouve differentes types de license, nous nous focaliserons plus sur les licenses -[Creative commons](https://creativecommons.org/about/cclicenses/) car elles donnent
la possibilité à d'autre de s'inspirer et d'utiliser notre travaille. Notre but est de partager, faire découvrir et amener à certaine innovation donc il est important de la laissé en creative license. 
 Parmis les -[Creative commons](https://creativecommons.org/about/cclicenses/) licenses il y'a plusieurs possibilitées tels que :

| Options |  significations   |  
|-----|------------------------------------------|
| BY  | credit must be given to the creator    |  
| SA  | Adaptations must be shared under the same terms    |   
| NC  | Only noncommercial uses of the work are permitted  |  
| ND  | No derivatives or adaptations of the work are permitted  |  

Donc il existe toute forme de license par combinaison des précédentes comme on peut retrouver sur le lien 

## 2.4 Ma Réalisation sur OpenSCAD

Commençons par divulger notre type de license. 

```
// File : lego_parfait.scad

// Author : Preux Karl

//Date : 25 février 2023

// license : [Creative Commons Attribution-ShareAlike 4.0 International CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```
J'ai décidé de creer un lego dont on introduit juste le nombre de ligne et de collone que l'on souhaite et la pièce se creer:



### simple 4 ligne , 2 colonne

```
 // ici il suifit d'introduire le nombre de colonne et de ligne voulue pour avoir un bon lego {
 
colonne_lego = 2;
ligne_lego = 4;

```
![](../fabzero-modules/images/lego_4_2.jpg)
### Paramétrisation
```
// parameter: dimension modifié et adapté à un vrai légo 

hauteur_c1 = 9.60;
hauteur_cil1 = 1.80; //hauteur du cilindre 1
rayon_cil = 2.42; // rayon des cylindre
rayon_cil2 = 3.25; //rayon cylindre intérieurdu cube 
colone= colonne_lego -1 ;//Nombre de colone de cylindre toujours +1
ligne = ligne_lego -1;//nombre de ligne de cylindre toujours +1
longueur_c1 = (ligne+1)*7.95;//31.80; //c1 correspond au cube 1
largeur_c1 = (colone+1)*7.9;
$fn =25;
```

### creer un cube ouvert 
```
difference(){
    cube([longueur_c1, largeur_c1, hauteur_c1]);
    translate([1.45,1.45])//centre le trou 
    cube([longueur_c1-2.90, largeur_c1-2.90, hauteur_c1-1]);
}
```
![](images/cube_1.jpg)

### boucle pour trou supérieur 
```
translate([3.90, 3.90])
for (j=[0:colone]){
    for (i = [0:ligne]){
        translate([i*8,j*8,hauteur_c1])
        cylinder(h=hauteur_cil1,r=rayon_cil);
        }
    }
```
![](../fabzero-modules/images/lego_4_2.jpg)

### boucle pour trou intérieur
Ici, on retrouve des cylindre placés à l'intérieure, lees cylindres sont également creux donc on réalise bien une difference entre deux cylindre.
```
translate([7.90,7.90])
for (k =[0:ligne-1]){ 
    for (t = [0:colone-1]){
        translate([k*8,t*8])
        difference(){
            cylinder(h=hauteur_c1-1, r=rayon_cil+0.8);
            cylinder(h=hauteur_c1-1, r=rayon_cil);
        }
    }
}
```
![](../fabzero-modules/images/cube_2.jpg)

A partir de là on peut s'amuser à modifier la pièce
1. par exemple, une 2x2 :

    ![](../fabzero-modules/images/cube_3.jpg)
    ![](../fabzero-modules/images/cube_6.jpg)

2. ou une 3X5 : 

    ![](../fabzero-modules/images/cube_4.jpg)
    ![](../fabzero-modules/images/cube_5.jpg)

## 2.5 Utilisation d'autres logisiel

ayant rélisé beaucoup de travaux sur solidworks, j'ai décidé d'utilisé OpenSCAD seulement mais FreeCAD ressemble énormément à solidworks mais un peu moins facile d'approche mais 
le principe reste le même et pareille sur Fusion360

1. choisir un plan 
2. construire une esquisse
3. lié les contraintes 
4. extrusion 
5. polir les bords
 

### mise en commun 

Pour mon flexlink en commun j'ai décider de prendre celui de Chiara Castrataro. Nous nous sommes partagés les différents code où j'ai pu rajouté une phrase tel que : 

```
// File : flexlink.scad

// Author : Preux Karl

//Date of modification : 25 février 2023

// From : Chiara Castrataro, piece1.stl last modification 27/02/2023

// license : [Creative Commons Attribution-ShareAlike 4.0 International CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```
qui signifie que ce code appartient à cette personne sous license créative.  

l'ajustement de sa pièce c'est fait rapidement car elle était bien paramétrisée. 