# 4. Outil Fab Lab sélectionné

Cette semaine est consacrée à l'apprentissage d'outils du Fab lab. Pour ma part, j'ai sélectionné le laser-cutters, donc une découpeuse laser. 

## 4.1 Règles à connaitre

Afin de performer durant la session, il a été demander de s'instruire sur les précausions à prendre, plusieures règles de bonne conduite sont décrite sur [Rules Laser-cutters](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md
).


### 4.1.1 Avant de débuter

Les différentes choses à savoir et à faire avant de débuter la découpe :

1. Connaitre avec certitude le type de matériau que l'on vas découper.

2. Toujours activer l'air comprimé à l'exception pour l'Epilog, ou ce processus est automatisé).

3. Toujours allumer l'extracteur de fumée.

4. Connaitre l'emplacement des boutons surtout ceelui d'arrêt d'urgence

5. Savoir où trouver un extincteur au CO_2.

### 4.1.2 Durant la découpe 
Les règles suivantes sont à respecter durant la découpe.

1. Porter des lunettes de protection adéquates. 

2. Ne pas regarder fixement l'impact du faisceau laser.

3. Rester à proximité de la machine jusqu'à la fin de la découpe. 

### 4.1.3 Après la découpe
Quand la découpe est fini, de nouvelles règles se rajouttent

1. Ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur.

2. Enlever les résidus de la découpe (si besoin, utiliser un aspirateur). 


### 4.1.4 Matériaux

Comme énoncer plus haut on retrouve les différents matérieux à traves les [Règles de la découpeuse laser](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md) , les matériaux recommandés et même déconseillés et interdits sont énoncer.  
En général, les plastiques sont déconseillés et parfois comme les métaux et cuire animal interdits par cause d'émanation de poussière ou fumée acide et nocive. 


## 4.2 Processus

Différents types de processus son possible avec cette machine, ces processus ce retrouve également à travers le lien de la [découpeuse laser](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md).

### 4.2.1 Découpe (vector)

La découpe se fait suivant un tracé vectoriel. Il faut donc importer un fichier vectoriel (par exemple au format .svg).
Pour les matériaux épais, il est souvent nécéssaire de faire plusieurs passages pour arriver à découper à travers.

### 4.2.2 Gravure vectorielle

Même technique que pour la découpe mais avec moins de puissance. Les traits sont donc gravés à une faible profondeur.

### 4.2.3 Gravure matricielle (engrave)

La gravure matricielle permet de graver un dessin à la surface du matériau.
Avec ce processus, la machine fonctionne comme une imprimante. Le dessin sera imprimé sous forme de trame.
La machine va adapter l'intensité du laser en fonction de la luminosité du dessin. Les parties sombres seront gravées plus fort que les parties claires.

## 4.3 Réglages

Les deux réglages indispensables sont la **vitesse** et **la puissance**. 

1. **Une puissance** élevée implique plus de chance de découper la matière en une passe, mais aussi plus de risque de brûler la matière! Donc par exemple pour un matériau épais et dense (comme du bois), on choisira une puissance élevée avec une faible vitesse. 

2. **Une vitesse** élevé implique que le travail sera fini plus vite, mais aussi un risque de ne pas couper à travers la matière. Par exemple pour un matériau fin (comme du papier ou du carton), on choisira une puissance faible avec une vitesse élevée.

Pour savoir quel est le réglage optimal pour votre matériau, consultez les grilles de test présentes au fablab.


S'il n'y a pas de grille de test correspondant à votre matériau, il est fortement recommandé de faire des tests vous-même avant de commencer une découpe plus conséquente.


## 4.4 Machines fablab 



| Macines |   Surface de découpe (cm) |  Hauteur maximum (cm) |  Puissance du Laser  (w) | Type de Laser  | Logiciel | Manuel |
|---------|---------------------------|-----------------------|--------------------------|----------------|----------|--------|
| Epilog fusion Pro 32  |  81x50   |  31 | 60 |   CO2 (infrarouge)   | Epilog Dashboard | [Epilog Fusion Pro 32](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md) |
| Lasersaur  | 120x60    |  12 | 100  |     CO2 (infrarouge)   | Driveboard App | [Lasersaur](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md) |
| Full Spectrum Muse   | 50x30  |  6 | 40  |  CO2 (infrarouge) + pointeur rouge | Retina Engrave| [Full Spectrum Muse](https://fslaser.com/material-test/) + [videos](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6)   |


## 4.5 Mise en pratique

### 4.5.1 First test
 Apres la présentation de trois machine, durant laquelle les élément important nous ont été remontrer tel que les filtres pour dégager la fumée, le comprosseur d'air pour stopper les flammes ou encore le refroidisseur pour réguler la temperature du laser. 

 Une courte expérience a été réaliser sur une des machines, mon groupe et moi avons réalisés une découpe sur la full spectrum muse, où notre ordinateur était nécessaire, nous l'avons donc connecter par wifi avec la découpeuse ensuite, on a introduit l'adresse ip de celle-ci dans le navigateur àfin d'ouvrir la page de réglage. Il est possible de visualiser la découpe avant c'est ce qui est fait àfin de bien placer le matériau à découper. 

 ![](../fabzero-modules/images/site_2.jpg)

 Ici on a un aperçu de notre découpe :

 ![](../fabzero-modules/images/IMG_4350.jpg)

 On a ici un vector cutting speed où la vitesse est mise en relation avec la puissance de la machine on voit que lorsque la puissance est grande et la vitesse trop faible le matériau est bruler.  


### 4.5.2 Second test

j'ai commencé par réaliser un brouillon sur la page Re3 de la découpeuse 3D, ici on voit l'exemple : 
  ![](../fabzero-modules/images/site_3.jpg)

ce qui permet de réalisé des forma comme celle-ci 

 ![](../fabzero-modules/images/piece.jpg)