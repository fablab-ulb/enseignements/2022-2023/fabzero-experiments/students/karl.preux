# 5. Dynamique de groupe et projet final

## 5.1 Project analysis and design 

L'objectif de ce premier point est d'étudier une problématique. La représenter sous forme d'un arbre dont le tronc est le problème principale, suivit des racines qui en sont les causes et les branches qui représentent elles les conséquences. 

La deuxième étape est de transformer l'arbre à problème en arbre à objectif, dont les racines deviennetn des étapes à réaliser et les branches restes des conséquences de cette objectif.

Sur base des 17 objectif des nations unis, on a élaboré une problématique sur le thème de l'accès des énergie durables et fiables pour tous. 

Cet arbre est réalisé conjointement avec [Chiara Castrataro](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/chiara.castrataro/fabzero-modules/module05/). 

![](../fabzero-modules/images/tree_1.jpg)


suivi de notre arbre à objectif

![](../fabzero-modules/images/tree_2.jpg)

L'accès à l'énergie durable et fiable, peut fournir une indépendance énergétique à beaucoup de pays et réduire les conflits et la hause des prix par exemple des pays en méditarrané pourrait plus utiliser des ressource à disposition comme le soleil, l'hydraulique mais aussi l'eolien et réduirer leur production ou besoin en énergie fossil. Pour moi, du a la difficulté de controle du renouvelable celui-ci devrait être couplé au nucléaire pour cela il faut permettre à tout les pays de bénéficier de cette technologie mais aussi du dévellopement de celle-ci car à l'heure actuelle il est possible de réduire ces déchets et d'utiliser de l'uranium faiblement enrichit. De plus, l'augmentation de cette technologie augmentera le dévellopement de celle-ci.

## 5.2 Etape 1: formation de groupe

Cette séance à servi à plus ou moins rentrer dans le sujet du projet dans le cadre de trouver une thématique et des problématiques lié à celle-ci.

### 5.2.1 Pré requis

Il a été demander de trouver un objet qui représente un thème ou un problème sociétale qui nous tient à coeur. 
Dans cette idée, j'ai pris une lampe qui représente l'accès à l'électricité mais aussi le gaspillage de ressource de première du à la surconsommation et donc le besoin à l'accès à lénergie avec peu en étant responsable de l'environment.

### 5.2.2 mise en commun

Autour d'un cercle chacun à pu présenté son objet et le thème auquel il est lié. Suite à ça des groupe se sont formé autour de thème plus ou moins similaire on retrouve le gaspillage et le recyclage dans le groupe que j'ai rejoint.
Malgré que se sujet soit vaste donc ouvre plein de porte à plein de penser différente. Ensuite une feuille sur laquelle des idée differentes qui émergait de chacun à été transcrite à fin de partager notre vision de la thématique et de l'ensemble des objets à fin de trouver la bonne thèmatique. Pour les groupes où le choix était plus dure, une sorte de charte à été distribué pour départager les différents thèmes et envie du groupe en fonction de j'aime le sujet, je suis la dynamique , cela ne me dérange pas et je n'apprécie pas du tout. 


Pour ma part le sujet de pollution n'est pas ma grande passion mais si ce recyclage peut être utilisé à des fins de production d'énergie verte le sujet m'enchante. Une séparation des procéssus pourra être réalisé dans l'avenir àfin que chacun choisissent l'étape qu'il l'enchante le plus. 

## 5.3 Etape 2 : Problématique

Des idée sous forme de "moi à ta place" on été proposée ausein du petit groupe, qui on pu ensuite récolté les idées de problématique par rapport à leur sujet vue par d'autres groupes. 

### 5.3.1 Idée ressorti

|  Idée   |  Problématique  | 
|-----------------|---------|
| Pollution    |  Déchet de matière première| 
| 			   |  Déchet dans l'ocean| 
| 			   |  Problème avec le plastique| 
|              |  l'efficacité des énergy renouvelable est faible et sa proportion est moindre coparer au autre| 
| Recyclage    |  l'efficacité du recyclage est faible et la pollution s'accroit| 
| 		       |  les matière première sont mal recycler | 
| 			   |  la surconsommation pousse à une production démsurrer| 

## 5.4 Group dynamics

On a appris durant cette seance différente manière de communiquer, les votes mais aussi la prise de parole et conscience par tous, le fait de mettre son ressenti sur le coté, être objectif et exprimer ces idées, on a décider d'utiliser le consensus avec un vote à degré pour répartir les differentes decission sur lesquelles on pourrait ne pas être d'accord.

Durant chaque réunion, l'idée, le sujet et la problématique sur laquelle nous discutterenont à été communiquée au paravant,  histoire de pouvoir réfléchir et venir avec des idées fraiches dans l'espoir d'être éfficace. Un membre s'occupe du temps de parole, un autre est secrétaire, et un autre s'occupe de savoir si tout le monde à pu exprimer son idée, il est médiateur. 
