## Foreword

Hello! Bienvenue dans mon student blog for the 2022-2023 ULB class **“How To Make (almost) Any Experiments Using Digital Fabrication”**.

Cette page va vous permettre de visualiser mon travail, ainsi que de me rencontrer à travers lui. Ici, une documentation et un descriptif de mes recherches et expériences sera réalisé.


## About me


![](images/profil_photo.jpg)
Bonjour! Moi c'est Karl. Je suis un étudiant à l'école polytechnique en MA2 électromecanique option energy. Dans cette option, je me suis spécialisé par le biais de mon projet MA1 et mon stage au sein de l'entreprise ENGIE impact sur les réseaux électriques, la régulation ainsi que les marchés. 

Visit this website to see my work!

## My background

Je suis né à Uccle, c'est également là où j'ai réalisé mes études de la primaire à la secondaire à l'école **Notre Dame des Champs**. C'est dans cette école que l'interêt pour les sciences et les mathématiques m'est apparru. 
J'ai également pratiqué du Judo au niveau national et du basketball au niveau régional durant de nombreuses années. 

## Previous work

En première année, j'ai réalié avec une équipe de 4 autres personnes un projet sur une voiture autonome, où nous avons réalisé l'assemblage de la voiture et le codage grâce à un arduino. La voiture devait être capable d'avancer et pouvoir suivre les panneaux des signalisations sur une route. 
En deuxième année, toujours en équipe mais de 8 cette fois ci, nous avons réalisé un bras bionique où la main pouvait se fermer tout en appliquant la force exacte afin de ne pas abimer l'objet. Durant ce projet, du codage sur arduino à également été necessaire pour activer des capteurs sensoriels et controler des servo moteurs.
En troisième année, j'ai réalisé un vibrequin sur solidworks.
En MA1, j'ai eu divers projets comme la régulation de chaleur d'un exchanger, le but étant de suivre une courbe de chauffe le plus rapidement possible. Ce projet a été réalisé grâce à des controlleurs numérique et des sensors analogiques.
J'ai également développé un système qui permet une régulation des fréquences grâce à des batteries dans un réseau électrique submergé d'énergie renouvelable. 

## Pourquoi avoir choisis "How to make any experiments " 

Parce que ce cours me permettait d'avoir un aspect plus pratique sur la matière, et je suis quelqu'un qui aime bien travailler la mécanique donc pouvoir concevoir, creer mes propres outils afin de réparer ou créer donc ce cours était une oportunité pour moi d'apprendre sur un savoir partagé. 



